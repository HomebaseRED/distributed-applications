/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package resourceGatherer.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import weatherDomain.WeatherRoot;

/**
 *
 * @author Robrecht Vanhuysse
 */
@Service
public class WeatherResourceCall implements ResourceCall{

    private static final String APPKEY = "&APPID=24f0f62f30d789639649712ac9407cd2";
    private static final String TARGETDATALOCATION = "./src/main/resources/zipcodes.txt";    
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private DiscoveryClient discoveryClient;
    private List<String> targetZipCodes;
    private String targetCountryCode;
    private int counter;
    private int counterMax;
    private Map<String, WeatherRoot> data;
    
    
    public WeatherResourceCall(RestTemplate rest){
        this.restTemplate = rest;
        loadTargetData();
        this.counter = 0;
        this.counterMax = targetZipCodes.size()/60;
        this.data = new HashMap<String, WeatherRoot>();
        System.out.println("weatherResourceCall constructed");
    }
    
    public RestTemplate getRestTemplate(){
        return this.restTemplate;
    }
    
    public void setRestTemplate(RestTemplate template){
        this.restTemplate = template;
    }
    
    public Map<String, WeatherRoot> getData()
    {
        return this.data;
    }
    
    private void loadTargetData() throws RuntimeException{
        File targetFile = new File(TARGETDATALOCATION);
        System.out.println(targetFile.getAbsolutePath());
        try(Scanner s = new Scanner(targetFile, "UTF-8")){
            boolean first = true;
            List<String> zipCodes = new ArrayList<String>();
            while (s.hasNextLine()){
                if(first){
                    this.targetCountryCode = s.next();
                    first = false;
                }else{
                    zipCodes.add(s.next());
                }
            }
            this.targetZipCodes = zipCodes;
        }catch(FileNotFoundException e){
            throw new RuntimeException(e);
        }       
    }
    
    @Override
    @Scheduled(fixedDelay = 1000)
    public void callResource() {
        List<String> limitedList = null;
        if(counter < counterMax){
            limitedList = targetZipCodes.subList(counter*60, (counter+1)*60);
            counter++;
        }else{
            limitedList = targetZipCodes.subList(counter*60, targetZipCodes.size());
            counter = 0;
        }
        for(String s : limitedList){
            String target = "http://api.openweathermap.org/data/2.5/weather?zip=" + s + "," + targetCountryCode + APPKEY;
            WeatherRoot report = restTemplate.getForObject(target, WeatherRoot.class);
            this.data.put(s, report);
        }
    }
    
    @Scheduled(fixedDelay = 60000)
    public void passData(){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<Map<String, WeatherRoot>> request = new HttpEntity<>(this.getData(), headers);
        restTemplate.postForLocation("https://WeatherAnalysisApi/weatherApi/submitDataSet", request);
    }
}
