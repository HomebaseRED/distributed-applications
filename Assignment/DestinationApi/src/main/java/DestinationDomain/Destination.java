/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DestinationDomain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author redro
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "zipCode",
    "name",
    "pointsOfInterest"
})
public class Destination {
    
    @JsonProperty("zipCode")
    private String zipCode;
    @JsonProperty("name")
    private String name;
    @JsonProperty("pointsOfInterest")
    private Map<String, String> pointsOfInterest;

    public Destination() {
        this.pointsOfInterest = new HashMap<String, String>();
    }

    public Destination(String zipCode, String name, Map<String, String> pointsOfInterest) {
        this.zipCode = zipCode;
        this.name = name;
        this.pointsOfInterest = pointsOfInterest;
    }

    @JsonProperty("zipCode")
    public String getZipCode() {
        return zipCode;
    }

    @JsonProperty("zipCode")
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("pointsOfInterest")
    public Map<String, String> getpointsOfInterest() {
        return pointsOfInterest;
    }

    @JsonProperty("pointsOfInterest")
    public void setpointsOfInterest(Map<String, String> pointsOfInterest) {
        this.pointsOfInterest = pointsOfInterest;
    }

    public void addpoint(String point, String url){
        this.pointsOfInterest.put(point, url);
    }
    
    public void removepoint(String point){
        this.pointsOfInterest.remove(point);
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.zipCode);
        hash = 83 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Destination other = (Destination) obj;
        if (!Objects.equals(this.zipCode, other.zipCode)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Destination{" + "zipCode=" + zipCode + ", name=" + name + ", pointsOfInterest=" + pointsOfInterest + '}';
    }    
}
