package DestinationService;

import DestinationDatabase.DestinationDatabaseInterface;
import DestinationDomain.Destination;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author redro
 */
@RestController
public class DestinationService {
    
    @Autowired
    private DestinationDatabaseInterface database;

    public DestinationService() {
    }

    public DestinationDatabaseInterface getDatabase() {
        return database;
    }

    public void setDatabase(DestinationDatabaseInterface database) {
        this.database = database;
    }
    
    @RequestMapping(value = "/destinations/{zipCode}", produces = {"application/json"}, method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getDestinationByZipCode(@PathVariable("zipCode") String zipCode){
        HashMap<String, Object> responseBody = new HashMap<>();
        Destination d = this.database.getDestinationByZipCode(zipCode);
        responseBody.put("destination", d);
        return new ResponseEntity<Map<String, Object>>(responseBody, HttpStatus.OK);
    }
    
}
