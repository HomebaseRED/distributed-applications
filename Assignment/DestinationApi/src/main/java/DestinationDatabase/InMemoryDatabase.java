package DestinationDatabase;

import DestinationDomain.Destination;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author redro
 */
public class InMemoryDatabase implements DestinationDatabaseInterface{

    private Set<Destination> database;

    public InMemoryDatabase() {
        this.database= new HashSet<Destination>();
        this.fill();
    }

    public InMemoryDatabase(Set<Destination> database) {
        this.database = database;
    }

    public Set<Destination> getDatabase() {
        return database;
    }

    public void setDatabase(Set<Destination> database) {
        this.database = database;
    }

    private void fill(){
        Map<String, String> AntPOI = new HashMap<String, String>();
        AntPOI.put("O.L.VrouweKathedraal", "http://www.dekathedraal.be/");
        Destination antwerpen = new Destination("2000", "Antwerpen", AntPOI);
        this.database.add(antwerpen);
        Destination Brussel = new Destination("1000", "Brussel", new HashMap<String, String>());
        this.database.add(Brussel);
        Map<String, String> LeuPOI = new HashMap<String, String>();
        LeuPOI.put("BibliotheekToren", "https://www.uitinvlaanderen.be/agenda/e/bezoek-de-bibliotheektoren-van-leuven/6412d8ec-043e-4a45-8bf5-332671700192");
        Destination leuven = new Destination("3000", "Leuven", LeuPOI);
        this.database.add(leuven);
    }
    
    @Override
    public Destination getDestinationByZipCode(String zipCode) {
        Destination response = null;
        for(Destination d : this.database){
            if(d.getZipCode().equals(zipCode)){
                response = d;
                break;
            }
        }
        return response;
    }

    @Override
    public void saveDestination(Destination d) {
        this.database.add(d);
    }
    
}
