/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DestinationDatabase;

import DestinationDomain.Destination;

/**
 *
 * @author redro
 */
public interface DestinationDatabaseInterface {
    
    Destination getDestinationByZipCode(String zipCode);
    void saveDestination(Destination d);
    
}
