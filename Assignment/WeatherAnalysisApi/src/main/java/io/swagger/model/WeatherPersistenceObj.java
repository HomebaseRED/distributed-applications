/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;
import weatherDomain.WeatherRoot;

/**
 *
 * @author Robrecht Vanhuysse
 */
@Entity
@Table
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "zipCode",
    "cityName",
    "timestamp",
    "report"
})
public class WeatherPersistenceObj implements Serializable{
    
    @JsonIgnore
    @Id
    @Column(name = "PKEY",nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long pkey;
    @JsonProperty("zipCode")
    @Column(name = "ZIPCODE")
    private String zipCode;
    @JsonProperty("cityName")
    @Column(name = "CITYNAME")
    private String cityName;
    @JsonProperty("timestamp")
    @Column(name = "TIMESTAMP")
    private long timestamp;
    @JsonProperty("report")
    @OneToOne(cascade = {CascadeType.ALL}, targetEntity = weatherDomain.WeatherRoot.class)
    @JoinColumn(name="PKEY")
    private WeatherRoot report;

    public WeatherPersistenceObj() {
    }

    public WeatherPersistenceObj(long id, String cityName, String zip, long timestamp, WeatherRoot report) {
        this.pkey = id;
        this.zipCode = zip;
        this.cityName = cityName;
        this.timestamp = timestamp;
        this.report = report;
    }

    public long getPkey() {
        return pkey;
    }

    public void setPkey(long id) {
        this.pkey = id;
    }

    @JsonProperty("zipCode")
    public String getZipCode() {
        return zipCode;
    }

    @JsonProperty("zipCode")
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    
    @JsonProperty("cityName")
    public String getCityName() {
        return cityName;
    }

    @JsonProperty("cityName")
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @JsonProperty("timestamp")
    public long getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
    
    @JsonProperty("report")
    public WeatherRoot getReport() {
        return report;
    }

    @JsonProperty("report")
    public void setReport(WeatherRoot report) {
        this.report = report;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (this.pkey ^ (this.pkey >>> 32));
        hash = 89 * hash + Objects.hashCode(this.zipCode);
        hash = 89 * hash + Objects.hashCode(this.cityName);
        hash = 89 * hash + (int) (this.timestamp ^ (this.timestamp >>> 32));
        hash = 89 * hash + Objects.hashCode(this.report);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WeatherPersistenceObj other = (WeatherPersistenceObj) obj;
        if (this.pkey != other.pkey) {
            return false;
        }
        if (this.timestamp != other.timestamp) {
            return false;
        }
        if (!Objects.equals(this.cityName, other.cityName)) {
            return false;
        }
        if (!Objects.equals(this.report, other.report)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WeatherPersistenceObj{" + "pkey=" + pkey + ", zipCode=" + zipCode + ", cityName=" + cityName + ", timestamp=" + timestamp + ", report=" + report.toString() + '}';
    }
    
    
    
}
