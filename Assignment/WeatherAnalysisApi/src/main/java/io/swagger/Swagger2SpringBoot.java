package io.swagger;

import io.swagger.database.DerbyHibernateRepository;
import io.swagger.database.InMemoryDatabase;
import io.swagger.database.RepositoryInterface;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableDiscoveryClient
@EnableSwagger2
@ComponentScan(basePackages = "io.swagger")
@EntityScan(basePackages = {"io.swagger", "weatherDomain"})
public class Swagger2SpringBoot implements CommandLineRunner {

	@Override
	public void run(String... arg0) throws Exception {
		if (arg0.length > 0 && arg0[0].equals("exitcode")) {
			throw new ExitException();
		}
	}

	public static void main(String[] args) throws Exception {
		new SpringApplication(Swagger2SpringBoot.class).run(args);
	}

	class ExitException extends RuntimeException implements ExitCodeGenerator {
		private static final long serialVersionUID = 1L;

		@Override
		public int getExitCode() {
			return 10;
		}

	}
        
//        @Bean
//        public EntityManagerFactory entityManagerFactory(){
//            return Persistence.createEntityManagerFactory("WeatherAnalysisApi_Hibernate_PU");
//        }
        
        @Bean
        public RepositoryInterface repositoryInterface(){
            return new InMemoryDatabase();
        }
}
