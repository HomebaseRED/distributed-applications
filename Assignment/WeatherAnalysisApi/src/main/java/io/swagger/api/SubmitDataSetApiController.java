package io.swagger.api;

import io.swagger.annotations.*;
import io.swagger.database.RepositoryInterface;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import weatherDomain.WeatherRoot;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-10-09T13:36:07.232Z")

@Controller
public class SubmitDataSetApiController implements SubmitDataSetApi {

    @Autowired
    private RepositoryInterface database;
    
    public SubmitDataSetApiController(){}
    
    public SubmitDataSetApiController(RepositoryInterface db){
        this.database = db;
    }
    
    public void setDatabase(RepositoryInterface db){
        this.database = db;
    }
    
    public RepositoryInterface getDatabase(){
        return this.database;
    }
    
    public ResponseEntity<Void> submitDataSetPost(@ApiParam(value = "current weatherData set"  ) @RequestBody Map<String, WeatherRoot> dataSet) {
        boolean resp = this.database.saveDataset(dataSet);
        if(resp){
            return new ResponseEntity<Void>(HttpStatus.OK);
        } else {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
