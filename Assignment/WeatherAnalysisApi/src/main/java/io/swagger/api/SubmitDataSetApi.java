package io.swagger.api;

import io.swagger.annotations.*;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import weatherDomain.WeatherRoot;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-10-09T13:36:07.232Z")

@Api(value = "submitDataSet", description = "the submitDataSet API")
public interface SubmitDataSetApi {

    @ApiOperation(value = "Entrypoint for data sent by ResourceGatherer", notes = "", response = Void.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Successfully posted current dataset to analysis system", response = Void.class),
        @ApiResponse(code = 400, message = "Failed post of the current dataset", response = Void.class) })
    @RequestMapping(value = "/submitDataSet",
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Void> submitDataSetPost(@ApiParam(value = "current weatherData set"  ) @RequestBody Map<String, WeatherRoot> dataSet);

}
