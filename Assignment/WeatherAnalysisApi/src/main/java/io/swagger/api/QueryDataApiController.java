package io.swagger.api;

import io.swagger.annotations.*;
import io.swagger.database.RepositoryInterface;
import io.swagger.model.WeatherPersistenceObj;
import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-10-09T13:36:07.232Z")

@Controller
public class QueryDataApiController implements QueryDataApi {

    @Autowired
    private RepositoryInterface database;
    
    public QueryDataApiController(){}
    
    public QueryDataApiController(RepositoryInterface db){
        this.database = db;
    }
    
    public void setDatabase(RepositoryInterface db){
        this.database = db;
    }
    
    public RepositoryInterface getDatabase(){
        return this.database;
    }
    
    public ResponseEntity<Map<String, Object>> queryDataAverageTemperatureCityNameGet(@ApiParam(value = "name of the requested city",required=true ) @PathVariable("cityName") String cityName) {
        double avgtemp = this.database.getAverageTemperatureFor(cityName);
        HashMap<String, Object> responseBody = new HashMap<>();
        responseBody.put("location", cityName);
        responseBody.put("temperature", avgtemp);
        return new ResponseEntity<Map<String, Object>>(responseBody, HttpStatus.OK);
    }

    public ResponseEntity<Map<String, Object>> queryDataLatestCityNameGet(@ApiParam(value = "name of the requested city",required=true ) @PathVariable("cityName") String cityName) {
        WeatherPersistenceObj data = this.database.getLatestReportFor(cityName);
        HashMap<String, Object> responseBody = new HashMap<>();
        responseBody.put("data", data);
        return new ResponseEntity<Map<String, Object>>(responseBody, HttpStatus.OK);
    }

}
