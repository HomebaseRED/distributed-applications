package io.swagger.api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Map;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-10-09T13:36:07.232Z")

@Api(value = "queryData", description = "the queryData API")
public interface QueryDataApi {

    @ApiOperation(value = "Returns most recent data on requested city", notes = "", response = Map.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Map.class),
        @ApiResponse(code = 400, message = "could not complete request", response = Map.class) })
    @RequestMapping(value = "/queryData/averageTemperature/{cityName}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Map<String, Object>> queryDataAverageTemperatureCityNameGet(@ApiParam(value = "name of the requested city",required=true ) @PathVariable("cityName") String cityName);


    @ApiOperation(value = "Returns most recent data on requested city", notes = "", response = Map.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Map.class),
        @ApiResponse(code = 400, message = "could not complete request", response = Map.class) })
    @RequestMapping(value = "/queryData/latest/{cityName}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Map<String, Object>> queryDataLatestCityNameGet(@ApiParam(value = "name of the requested city",required=true ) @PathVariable("cityName") String cityName);

}
