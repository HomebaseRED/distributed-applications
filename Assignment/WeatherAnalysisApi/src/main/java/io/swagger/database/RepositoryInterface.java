/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.swagger.database;

import io.swagger.model.WeatherPersistenceObj;
import java.util.Map;
import weatherDomain.WeatherRoot;



/**
 *
 * @author Robrecht Vanhuysse
 */
public interface RepositoryInterface {
    public boolean saveDataset(Map<String, WeatherRoot> data);
    public double getAverageTemperatureFor(String cityName);
    public WeatherPersistenceObj getLatestReportFor(String cityName);
}
