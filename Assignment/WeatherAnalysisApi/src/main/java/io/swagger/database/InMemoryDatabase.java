/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.swagger.database;

import io.swagger.model.WeatherPersistenceObj;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import weatherDomain.WeatherRoot;

/**
 *
 * @author redro
 */
public class InMemoryDatabase implements RepositoryInterface{

    private List<WeatherPersistenceObj> database;

    public InMemoryDatabase() {
        database = new ArrayList<WeatherPersistenceObj>();
    }

    public List<WeatherPersistenceObj> getDatabase() {
        return database;
    }

    public void setDatabase(List<WeatherPersistenceObj> database) {
        this.database = database;
    }
    
    
    
    @Override
    public boolean saveDataset(Map<String, WeatherRoot> data) {
        boolean response = true;
        for(Entry e : data.entrySet()){
            String zip = (String) e.getKey();
            WeatherRoot report = (WeatherRoot) e.getValue();
            Date time = new Date();
            WeatherPersistenceObj obj = new WeatherPersistenceObj();
            obj.setZipCode(zip);
            obj.setCityName(report.getName());
            obj.setTimestamp(time.getTime()*1000);
            obj.setReport(report);
            database.add(obj);
        }   
        return response;
    }

    @Override
    public double getAverageTemperatureFor(String cityName) {
        double avg = 0.0;
        double count = 0.0;
        if(cityName.equals("global")){
            for(WeatherPersistenceObj obj : this.getDatabase()){
                count++;
                avg += obj.getReport().getMain().getTemp();
            }
        }else{
            for(WeatherPersistenceObj obj : this.getDatabase()){
                if(obj.getCityName().equalsIgnoreCase(cityName)){
                    count++;
                    avg += obj.getReport().getMain().getTemp();
                }
            }
        }
        if(count > 0){
            avg = avg/count;
        }else{
            throw new IllegalArgumentException("City not found.");
        }
        return avg;
    }

    @Override
    public WeatherPersistenceObj getLatestReportFor(String cityName) {
        WeatherPersistenceObj response = null;
        long time = 0;
        for(WeatherPersistenceObj obj : this.getDatabase()){
            if(obj.getCityName().equalsIgnoreCase(cityName) && obj.getTimestamp() > time){
                time = obj.getTimestamp();
                response = obj;
            }
        }
        if(response == null){
            throw new IllegalArgumentException("City not found");
        }
        return response;
    }
    
}
