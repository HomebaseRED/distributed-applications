/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package io.swagger.database;

import io.swagger.model.WeatherPersistenceObj;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import javax.persistence.*;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import weatherDomain.WeatherRoot;

/**
 *
 * @author Robrecht Vanhuysse
 */
public class DerbyHibernateRepository implements RepositoryInterface{
    
    //@Autowired
    EntityManagerFactory emFactory;
    //@PersistenceContext(name = "WeatherAnalysisApi_Hibernate_PU")
    EntityManager manager;
    
    public DerbyHibernateRepository(){
        this.emFactory= Persistence.createEntityManagerFactory("WeatherAnalysisApi_Hibernate_PU");
    }
    
    public void close(){
        try{
            manager.close();
        } finally {
            emFactory.close();
        }
    }
    
//    public DerbyHibernateRepository(EntityManagerFactory emf){
//        this.emFactory = emf;
//    }
//
    public EntityManagerFactory getEmFactory() {
        return emFactory;
    }
    
    public void setEmFactory(EntityManagerFactory emFactory) {
        this.emFactory = emFactory;
    }
    
    public EntityManager getManager() {
        return manager;
    }
    
    public void setManager(EntityManager manager) {
        this.manager = manager;
    }
    
    @Override
//    @Transactional
    public boolean saveDataset(Map<String, WeatherRoot> data) {
        boolean response = true;
        for(Entry e : data.entrySet()){
            try{
                this.manager = emFactory.createEntityManager();
                manager.getTransaction().begin();
                String zip = (String) e.getKey();
                WeatherRoot report =  (WeatherRoot) e. getValue();
                Date time = new Date();
                WeatherPersistenceObj obj = new WeatherPersistenceObj();
                obj.setZipCode(zip);
                obj.setCityName(report.getName());
                obj.setTimestamp(time.getTime()*1000);
                obj.setReport(report);
                manager.persist(obj);
                manager.flush();
                manager.getTransaction().commit();
            }catch(Exception ex){
                manager.getTransaction().rollback();
                throw new RuntimeException(ex);
            }finally{
                manager.close();
            }
        }
//        manager.close();
return response;
    }
    
    @Override
    public double getAverageTemperatureFor(String cityName) {
        Double avg = 0.0;
        try{
            this.manager = emFactory.createEntityManager();
            manager.getTransaction().begin();
            if(cityName.equals("global")){
                TypedQuery<Double> query = manager.createQuery("SELECT AVG(mains.temp) FROM Main mains ", Double.class);
                avg = query.getSingleResult();
                manager.flush();
                manager.getTransaction().commit();
            } else {
                TypedQuery<Double> query = manager.createQuery("SELECT AVG(mains.temp) FROM WeatherRoot AS roots INNER JOIN roots.main AS mains WHERE roots.name = :cityName", Double.class);
                avg = query.setParameter("cityName", cityName).getSingleResult();
                manager.flush();
                manager.getTransaction().commit();
            }
        }catch(Exception ex){
            manager.getTransaction().rollback();
            throw new RuntimeException(ex);
        }finally{
            manager.close();
        }
        return avg;
    }
    
    @Override
    public WeatherPersistenceObj getLatestReportFor(String cityName) {
        WeatherPersistenceObj response = null;
        try{
            this.manager = emFactory.createEntityManager();
            manager.getTransaction().begin();
            TypedQuery<WeatherPersistenceObj> query = manager.createQuery("FROM WeatherPersistenceObj AS obj WHERE obj.timestamp = (SELECT MAX(obj2.timestamp) FROM WeatherPersistenceObj AS obj2 WHERE obj2.cityName LIKE "+"'%" + cityName + "%'"+" )", WeatherPersistenceObj.class);
            //query.setParameter(":param1", "'%" + cityName + "%'");
            response = query.getSingleResult();
        }catch(Exception ex){
            manager.getTransaction().rollback();
            throw new RuntimeException(ex);
        }finally{
            manager.close();
        }
        return response;
    }
    
    
}
