/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compositeModel;

import DestinationDomain.Destination;
import ReviewDomain.Review;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.model.WeatherPersistenceObj;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author redro
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "destination",
    "reviews",
    "weatherReport",
    "averageTemp"
})
public class CompositeObject {
    
    @JsonProperty("destination")
    private Destination destination;
    @JsonProperty("reviews")
    private List<Review> reviews;
    @JsonProperty("weatherReport")
    private WeatherPersistenceObj weatherReport;
    @JsonProperty("averageTemp")
    private double averageTemp;

    public CompositeObject() {
    }

    @JsonProperty("destination")
    public Destination getDestination() {
        return destination;
    }

    @JsonProperty("destination")
    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    @JsonProperty("reviews")
    public List<Review> getReviews() {
        return reviews;
    }

    @JsonProperty("reviews")
    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    @JsonProperty("reviews")
    public WeatherPersistenceObj getWeatherReport() {
        return weatherReport;
    }

    @JsonProperty("reviews")
    public void setWeatherReport(WeatherPersistenceObj weatherReport) {
        this.weatherReport = weatherReport;
    }

    @JsonProperty("averageTemp")
    public double getAverageTemp() {
        return averageTemp;
    }

    @JsonProperty("averageTemp")
    public void setAverageTemp(double averageTemp) {
        this.averageTemp = averageTemp;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.destination);
        hash = 41 * hash + Objects.hashCode(this.reviews);
        hash = 41 * hash + Objects.hashCode(this.weatherReport);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CompositeObject other = (CompositeObject) obj;
        if (!Objects.equals(this.destination, other.destination)) {
            return false;
        }
        if (!Objects.equals(this.reviews, other.reviews)) {
            return false;
        }
        if (!Objects.equals(this.weatherReport, other.weatherReport)) {
            return false;
        }
        return true;
    }
    
    
    
}
