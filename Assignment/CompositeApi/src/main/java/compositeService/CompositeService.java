/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compositeService;

import DestinationDomain.Destination;
import ReviewDomain.Review;
import compositeDatabase.CityDatabaseInterface;
import compositeModel.CompositeObject;
import io.swagger.model.WeatherPersistenceObj;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author redro
 */
@RestController
public class CompositeService {
    
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private CityDatabaseInterface database;
    
    
    public CompositeService(){}

    public CompositeService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    
    @RequestMapping(value="/TourismService/name/{cityName}", produces={"application/json"}, method=RequestMethod.GET )
    public ResponseEntity<CompositeObject> getDataByCityName(@PathVariable("cityName") String cityName){
        String zipCode = this.database.nameToZipCode(cityName);
        return this.getData(cityName, zipCode);
    }
    
    @RequestMapping(value="/TourismService/zip/{zipCode}", produces={"application/json"}, method=RequestMethod.GET )
    public ResponseEntity<CompositeObject> getDataByZipCode(@PathVariable("zipCode") String zipCode){
        String cityName = this.database.zipCodeToName(zipCode);
        return this.getData(cityName, zipCode);
    }
    
    public ResponseEntity<CompositeObject> getData(String cityName, String zipCode){
        CompositeObject response = new CompositeObject();
        String url = null;
        Destination destination = null;
        List<Review> reviews = null;
        WeatherPersistenceObj report = null;
        double avgTemp = 0.0;
        url = "http://DestinationApi/destinations/"+zipCode;
        ResponseEntity<Map> destinationResponse = restTemplate.getForEntity(url, Map.class);
        if(destinationResponse.getStatusCode() == HttpStatus.OK){
            Map<String, Object> responseBody = destinationResponse.getBody();
            destination = (Destination) responseBody.get("destination");
            if(destination != null){
                response.setDestination(destination);
            }
        }
        url ="http://ReviewApi/reviews/"+zipCode;
        ResponseEntity<Map> reviewsResponse = restTemplate.getForEntity(url, Map.class);
        if(reviewsResponse.getStatusCode() == HttpStatus.OK){
            Map<String, Object> responseBody = destinationResponse.getBody();
            reviews = (List<Review>) responseBody.get("reviews");
            if(reviews != null){
                response.setReviews(reviews);
            }
        }
        url = "http://WeatherAnalysisApi/queryData/latest/"+cityName;
        ResponseEntity<Map> weatherResponse = restTemplate.getForEntity(url, Map.class);
        if(weatherResponse.getStatusCode() == HttpStatus.OK){
            Map<String, Object> responseBody = weatherResponse.getBody();
            report = (WeatherPersistenceObj) responseBody.get("data");
            if(report != null){
                response.setWeatherReport(report);
            }
        }
        url = "http://WeatherAnalysisApi/queryData/averateTemperature/"+cityName;
        ResponseEntity<Map> temperatureResponse = restTemplate.getForEntity(url,Map.class);
        if(temperatureResponse.getStatusCode() == HttpStatus.OK){
            Map<String, Object> responseBody = temperatureResponse.getBody();
            avgTemp = (Double)responseBody.get("temperature");
            response.setAverageTemp(avgTemp);
        }
        return new ResponseEntity<CompositeObject>(response, HttpStatus.OK);
    }
    
}
