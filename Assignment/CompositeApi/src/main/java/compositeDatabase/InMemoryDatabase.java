/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compositeDatabase;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author redro
 */
public class InMemoryDatabase implements CityDatabaseInterface{
    
    private Map<String,String> zipToName;
    private Map<String,String> nameToZip;

    public InMemoryDatabase(){
        this.nameToZip = new HashMap<String, String>();
        this.zipToName = new HashMap<String, String>();
        this.fill();
    }

    public Map<String, String> getZipToName() {
        return zipToName;
    }

    public void setZipToName(Map<String, String> zipToName) {
        this.zipToName = zipToName;
    }

    public Map<String, String> getNameToZip() {
        return nameToZip;
    }

    public void setNameToZip(Map<String, String> nameToZip) {
        this.nameToZip = nameToZip;
    }
    
    public void fill(){
        nameToZip.put("Brussel", "1000");
        nameToZip.put("Antwerpen", "2000");
        nameToZip.put("Gent", "9000");
        nameToZip.put("Hasselt", "3500");
        nameToZip.put("Brugge", "8000");
        nameToZip.put("Leuven", "3000");
        
        zipToName.put("1000", "Brussel");
        zipToName.put("2000", "Antwerpen");
        zipToName.put("9000", "Gent");
        zipToName.put("3000", "Leuven");
        zipToName.put("3500", "Hasselt");
        zipToName.put("8000", "Brugge");
    }
    
    @Override
    public String zipCodeToName(String zipCode) {
        return this.zipToName.get(zipCode);
    }

    @Override
    public String nameToZipCode(String cityName) {
        return this.nameToZip.get(cityName);
    }
    
}
