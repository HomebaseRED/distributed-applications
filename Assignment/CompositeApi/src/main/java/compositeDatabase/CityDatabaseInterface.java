/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compositeDatabase;

/**
 *
 * @author redro
 */
public interface CityDatabaseInterface {
    
    String zipCodeToName(String zipCode);
    String nameToZipCode(String cityName);
    
}
