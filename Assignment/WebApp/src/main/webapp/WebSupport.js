
function fetchByZip(){
    var targetValue = $("#zipCode").val();
    var targetUrl = "http://localhost:8765/CompositeApi/TourismService/zip/";
    fetch(targetValue, targetUrl);
}

function fetchByName(){
    var targetValue = $("#cityName").val();
    var targetUrl = "http://localhost:8765/CompositeApi/TourismService/name/";
    fetch(targetValue, targetUrl);
}

function fetch(targetValue, targetUrl){
    targetUrl += targetValue;
    $.ajax({
        url:targetUrl,
        type: "GET"
    })
            .done(function(data){
                $("#destinationSection").empty();
                if(data.destination != null){
                    var zip = $("<p></p>").text(data.destination.zipCode);
                    var name = $("<p></p>").text(data.destination.name);
                    var list = $("<ul></ul>");
                    $("#destinationSection").append(zip, name);
                    $.each(data.destination.pointsOfInterest, function(key, value){
                        var element = $("<li></li>").text(key + " - " + value);
                        list.append(element);
                    });
                    $("#destinationSection").append(list);
                }else{
                    var mssg = $("<p></p>").text("This data could not be loaded");
                    $("#destinationSection").append(mssg);
                }
                $("#reviewSection").empty();
                if(data.reviews != null){
                    $.each(data.reviews, function(index, value){
                        var rev = $("<div></div>");
                        var usr = $("<p></p>").text(value.userName);
                        var comm = $("<p></p>").text(value.comment);
                        rev.append(usr, comm);
                        $("#reviewSection").append(rev); 
                    });
                }else{
                    var mssg = $("<p></p>").text("This data could not be loaded");
                    $("#reviewSection").append(mssg);
                }
                $("#weatherSection").empty();
                if(data.weatherReport != null){
                    var report = $("<p></p>").text(data.weatherReport.report);
                    var avg = $("<p></p>").text("Average Temperature: "+ data.averageTemp);
                }else{
                    var mssg = $("<p></p>").text("This data could not be loaded");
                    $("#weatherSection").append(mssg);
                }
            })
            .fail(function(xhr, ajaxOptions, thrownError){
                $("#error").text("The service failed to respond correctly. Please try again later or contact the service administartor.")
            })
    ;
}
