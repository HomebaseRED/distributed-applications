
package weatherDomain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "coord",
    "weather",
    "base",
    "main",
    "wind",
    "clouds",
    "rain",
    "dt",
    "sys",
    "id",
    "name",
    "cod"
})
public class WeatherRoot implements Serializable
{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PKEY")
    @JsonIgnore
    private long pkey;

    @JsonProperty("coord")
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="PKEY")
    private Coord coord;
    @JsonProperty("weather")
    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name="PKEY")
    private List<Weather> weather = null;
    @JsonProperty("base")
    @Column(name = "BASE")
    private String base;
    @JsonProperty("main")
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="PKEY")
    private Main main;
    @JsonProperty("wind")
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="PKEY")
    private Wind wind;
    @JsonProperty("clouds")
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="PKEY")
    private Clouds clouds;
    @JsonProperty("rain")
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="PKEY")
    private Rain rain;
    @JsonProperty("dt")
    @Column(name = "DT")
    private Integer dt;
    @JsonProperty("sys")
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="PKEY")
    private Sys sys;
    @JsonProperty("id")
    @Column(name = "ID")
    private Integer id;
    @JsonProperty("name")
    @Column(name = "NAME")
    private String name;
    @JsonProperty("cod")
    @Column(name = "COD")
    private Integer cod;
    @JsonIgnore
    @Transient
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -6888331215974648197L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public WeatherRoot() {
    }

    /**
     * 
     * @param id
     * @param dt
     * @param clouds
     * @param coord
     * @param wind
     * @param cod
     * @param sys
     * @param name
     * @param base
     * @param weather
     * @param rain
     * @param main
     */
    public WeatherRoot(Coord coord, List<Weather> weather, String base, Main main, Wind wind, Clouds clouds, Rain rain, Integer dt, Sys sys, Integer id, String name, Integer cod) {
        super();
        this.coord = coord;
        this.weather = weather;
        this.base = base;
        this.main = main;
        this.wind = wind;
        this.clouds = clouds;
        this.rain = rain;
        this.dt = dt;
        this.sys = sys;
        this.id = id;
        this.name = name;
        this.cod = cod;
    }

    @JsonProperty("coord")
    public Coord getCoord() {
        return coord;
    }

    @JsonProperty("coord")
    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    @JsonProperty("weather")
    public List<Weather> getWeather() {
        return weather;
    }

    @JsonProperty("weather")
    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    @JsonProperty("base")
    public String getBase() {
        return base;
    }

    @JsonProperty("base")
    public void setBase(String base) {
        this.base = base;
    }

    @JsonProperty("main")
    public Main getMain() {
        return main;
    }

    @JsonProperty("main")
    public void setMain(Main main) {
        this.main = main;
    }

    @JsonProperty("wind")
    public Wind getWind() {
        return wind;
    }

    @JsonProperty("wind")
    public void setWind(Wind wind) {
        this.wind = wind;
    }

    @JsonProperty("clouds")
    public Clouds getClouds() {
        return clouds;
    }

    @JsonProperty("clouds")
    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    @JsonProperty("rain")
    public Rain getRain() {
        return rain;
    }

    @JsonProperty("rain")
    public void setRain(Rain rain) {
        this.rain = rain;
    }

    @JsonProperty("dt")
    public Integer getDt() {
        return dt;
    }

    @JsonProperty("dt")
    public void setDt(Integer dt) {
        this.dt = dt;
    }

    @JsonProperty("sys")
    public Sys getSys() {
        return sys;
    }

    @JsonProperty("sys")
    public void setSys(Sys sys) {
        this.sys = sys;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("cod")
    public Integer getCod() {
        return cod;
    }

    @JsonProperty("cod")
    public void setCod(Integer cod) {
        this.cod = cod;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("coord", coord).append("weather", weather).append("base", base).append("main", main).append("wind", wind).append("clouds", clouds).append("rain", rain).append("dt", dt).append("sys", sys).append("id", id).append("name", name).append("cod", cod).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(dt).append(clouds).append(wind).append(sys).append(main).append(id).append(coord).append(cod).append(additionalProperties).append(name).append(base).append(weather).append(rain).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof WeatherRoot) == false) {
            return false;
        }
        WeatherRoot rhs = ((WeatherRoot) other);
        return new EqualsBuilder().append(dt, rhs.dt).append(clouds, rhs.clouds).append(wind, rhs.wind).append(sys, rhs.sys).append(main, rhs.main).append(id, rhs.id).append(coord, rhs.coord).append(cod, rhs.cod).append(additionalProperties, rhs.additionalProperties).append(name, rhs.name).append(base, rhs.base).append(weather, rhs.weather).append(rain, rhs.rain).isEquals();
    }

}
